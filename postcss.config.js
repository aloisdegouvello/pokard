module.exports = {
  plugins: {
    tailwindcss: {},
    "vue-cli-plugin-tailwind/purgecss": {
      content: ["./src/**/*.html", "./src/**/*.vue"],
      defaultExtractor: content =>
        content.match(/[A-Za-z0-9-_/:]*[A-Za-z0-9-_/]+/g) || []
    },
    autoprefixer: {}
  }
};
