export interface CardFormat {
  width: number;
  height: number;
  name: string;
  usage: string[];
}

export type MagnumGold = CardFormat;
export type MagnumSilver = CardFormat;
export type MagnumCopper = CardFormat;
export type StdCardUsaGame = CardFormat;
export type EuroSize = CardFormat;
export type Chimera = CardFormat;
export type StdUsa = CardFormat;
export type MiniEuro = CardFormat;
export type MiniChimera = CardFormat;
export type MiniUsa = CardFormat;

export interface CardFormats {
  magnumGold: MagnumGold;
  magnumSilver: MagnumSilver;
  magnumCopper: MagnumCopper;
  stdCardUsaGame: StdCardUsaGame;
  euroSize: EuroSize;
  chimera: Chimera;
  stdUsa: StdUsa;
  miniEuro: MiniEuro;
  miniChimera: MiniChimera;
  miniUsa: MiniUsa;
}
